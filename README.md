<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [EU-maiden liput](#eu-maiden-liput)
  - [Latominen](#latominen)
- [Flags of EU-countries](#flags-of-eu-countries)
  - [Building](#building)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# EU-maiden liput

Kuntouttavaa työtoimintaa varten yksinkertainen asiakirja EU-maiden lipuilla,
jotka voidaan tulostaa ja leikata Eurooppa-päivää varten.

## Latominen

1. Asenna `git`, `git-lfs` ja $\LaTeX$ -ympäristö, suosittelen `texlive-full`
   -pakettia tai vastaavaa.
2. Kloonaa tämä repo.
3. Aja `pdflatex projekti.tex`
4. `projekti.pdf` pitäisi nyt olla päivitetty.

Jos asennat `git-lfs` jälkikäteen, suorita `git-lfs pull`.

# Flags of EU-countries

Simple document for rehabitative employment with EU country flags for printing
and cutting up for Europe day.

## Building

1. Install `git`, `git-lfs` and a $\LaTeX$ environment, I recommend
   `texlive-full` package or equivalent.
2. Clone this repository.
3. Run `pdflatex projekti.tex`
4. `project.pdf` should now be updated

If you are installing `git-lfs` later, run `git-lfs pull`.
